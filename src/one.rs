trait One {
    fn one() -> Self;

    fn is_one(&self) -> bool
        where Self: Eq
    {
        *self == Self::one()
    }
}

#[derive(Debug)]
struct Boo(i32);

impl One for Boo {
    fn one() -> Self {
        Boo(1)
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Bar(i32);

impl One for Bar {
    fn one() -> Self {
        Bar(1)
    }
}

fn main() {
    println!("{:?}", Boo::a());
    // Boo::a().b()

    println!("{:?}", Bar::a().b());
}
